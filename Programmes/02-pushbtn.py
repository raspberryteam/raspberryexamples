#!/usr/bin/env python
import RPi.GPIO as GPIO

# Déclaration des emplacements
ButtonPin = 18
GreenLedPin = 23
RedLedPin = 24

# Mode de lecture des pins
mode = GPIO.BCM         #Lecture des pin par "nom"
#mode = GPIO.BOARD      #Lecture des pins par emplacement

# Compteur
i = 0

# Différents paramétrages pour nos pins
def setup():
    GPIO.setwarnings(False)
    GPIO.setmode(mode)
    # GPIO d'entrée
    GPIO.setup(ButtonPin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    # GPIO de sortie
    GPIO.setup(RedLedPin, GPIO.OUT, initial=GPIO.LOW)        # Initialise LED Rouge Eteinte
    GPIO.setup(GreenLedPin, GPIO.OUT, initial=GPIO.LOW)      # Initialise LED Verte Eteinte

# Boucle du programme pour intércepter les appels du bouttons poussoir
def loop():
    # Ecoute le pin, et dirige sur le bon callback
    # FALLING: HIGH to LOW
    # RAISING: LOW to HIGH
    # bouncetime permet de ne pas detecter l'événement jusqu'à 200ms après en avoir reçu un
    GPIO.add_event_detect(ButtonPin, GPIO.FALLING, callback=callbackBtn, bouncetime=200)
    while True:
        pass   # Ne rien faire

# Callback appelé lorsque nous appuyons sur le boutton poussoir
def callbackBtn(ev=None):
    # On compte le nombre de fois que l'événement est appelé
    global i
    i += 1
    print(" ___________________________")
    print ("|"+ repr(i) + "/ Pression sur le boutton |")
    swLed()

# Méthode appelée par le callback attendant une action sur le boutton
def swLed(ev=None):
    # Lecture de la LED (LOW = éteint // HIGH = allumé)
    RedLedStatus = GPIO.input(RedLedPin)
    # On change l'état des LED
    if RedLedStatus == GPIO.HIGH:
        print ('  - On éteint la rouge')
        GPIO.output(RedLedPin, GPIO.LOW)
        print ('  - On allume la verte')
        GPIO.output(GreenLedPin, GPIO.HIGH)
        print (" _________________________")
    else:
        print ('  - On allume la rouge')
        GPIO.output(RedLedPin, GPIO.HIGH)
        print ('  - On éteint la verte')
        GPIO.output(GreenLedPin, GPIO.LOW)
        print (" _________________________")

# Interruption du programme
def destroy():
    # On éteint les LED
    GPIO.output(RedLedPin, GPIO.LOW)
    GPIO.output(GreenLedPin, GPIO.LOW)
    # Relâcher les ressources
    GPIO.cleanup()

# Le programme démarre ici
if __name__ == '__main__':
    setup()
    try:
        loop()
    except KeyboardInterrupt:  # Si on interrompt le programme, on passe d'abord par la fonction destroy
        destroy()
